# pro-saude-vue

## Sobre a aplicação Pró-Saúde

Trata-se de uma aplicação web pública para o gerenciamento de recursos do sistema de gerência para clínicas Pró-Saúde. Por padrão ela consome os recursos da API pro-saude: https://gitlab.com/pro-saude-team/pro-saude-api.

Após clonar este repositório, entre na pasta do projeto e edite o arquivo localizado em src/services/config.js. Set a variável baseURL com a url base da API. Ex.: baseURL: 'http://localhost/pro-saude/public/api/v1/'.

Assumindo que você tem o node.js e o yarn instalados, basta seguir os comandos abaixo.

## Configuração do projeto
```
yarn install
```

### Teste para desenvolvedores
```
yarn run serve
```

### Colocando em produção
```
yarn run build
```
