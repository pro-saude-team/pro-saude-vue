import { http } from './config'

export default	{

    store:(clinic)=>{
        return http.post('clinics', clinic);
    },

    update:(clinic)=>{
        return http.put('clinics/' + clinic.id, clinic);
    },

    index:()=>{
        return http.get('clinics')
    },
    
    destroy:(clinic)=>{
        return http.delete('clinics/' + clinic.id)
	}
}
