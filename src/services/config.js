import axios from 'axios'

export const http = axios.create({
    baseURL: 'http://localhost/pro-saude/public/api/v1/',
    headers: {}
});
